#!/bin/bash
# Author: Jayden Kyaw Htet Aung (Regional Cloud Security Architect, CHKP)
# shiftleft binary should be added to env

# Docker Image
image="demo-app1"

# If you use AWS SSM to store CloudGuard API Key and Secret. Edit the values accordingly.
export CHKP_CLOUDGUARD_ID=$(aws ssm get-parameter --name "CHKP_CLOUDGUARD_ID" | jq -r '.Parameter.Value')
export CHKP_CLOUDGUARD_SECRET=$(aws ssm get-parameter --name "CHKP_CLOUDGUARD_SECRET" | jq -r '.Parameter.Value')


echo Scan started on `date`
echo Saving docker image to tar file

# Saving the image to a tar file
docker save $image -o ${image}-file.tar

# Scan the tar file, and export the results to result.txt
shiftleft image-scan -i ${image}-file.tar > result.txt

# Check if there are CVEs in the result
if grep -q CVE result.txt
then
  echo Vulnerabilities found in the docker image
  echo You should stop the pipeline.
else
  echo Your docker image is clean. 
fi

echo Scan finished on `date`
